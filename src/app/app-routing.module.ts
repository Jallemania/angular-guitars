import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";
import { GuitarCataloguePage } from "./pages/guitar-catalogue/guitar-catalogue.page";
import { LoginPage } from "./pages/login/login.page";
import { ProfilePage } from "./pages/profile/profile.page";

const routes: Routes = [
    {
        path: "", // path = no '/'
        pathMatch: "full",
        redirectTo: "/login", // redirect = '/path'
    },
    {
        path: "login",
        component: LoginPage,
    },
    {
        path: "guitars",
        component: GuitarCataloguePage,
        canActivate: [ AuthGuard ] //redirect if not logged in
    },
    {
        path: "profile",
        component: ProfilePage,
        canActivate: [ AuthGuard ] //redirect if not logged in
    }
]


@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ], //import modules
    exports: [
        RouterModule
    ] //expose modules
})
export class AppRoutingModule {}