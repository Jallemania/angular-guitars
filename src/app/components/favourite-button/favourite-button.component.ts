import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { FavouriteService } from 'src/app/services/favourite.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-favourite-button',
  templateUrl: './favourite-button.component.html',
  styleUrls: ['./favourite-button.component.css']
})
export class FavouriteButtonComponent implements OnInit {

  public _loading: boolean = false;

  public isFavourite: boolean = false;

  @Input() guitarId: string = "";

  constructor(
    private readonly  userService: UserService,
    private readonly favouriteService: FavouriteService,
  ) { }

  ngOnInit(): void {
    this.isFavourite = this.userService.inFavourites(this.guitarId);
  }

  onFavouriteClick(): void {
    this._loading = true;
    this.favouriteService.addToFavourites(this.guitarId)
      .subscribe({
        next: (user: User) => {
          this._loading = false;
          this.isFavourite = this.userService.inFavourites(this.guitarId);
        },
        error: (error: HttpErrorResponse) => {
          console.log("ERROR: ", error.message);
          
        }
      })
  }
}
