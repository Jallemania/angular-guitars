export const environment = {
  production: true,
  apiUsers: "https://mj-noroff-api.herokuapp.com/users",
  apiGuitars: "https://mj-noroff-api.herokuapp.com/guitars"
};
